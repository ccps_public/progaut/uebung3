extern "C" {
#include "parse_numbers.h"
}
#include <gtest/gtest.h>

TEST(parse_uint, valid_numbers) {
    uint32_t parsed = 42;

    EXPECT_TRUE(parse_uint(&parsed, "0"));
    EXPECT_EQ(parsed, 0);

    parsed = 42;
    EXPECT_TRUE(parse_uint(&parsed, "0000"));
    EXPECT_EQ(parsed, 0);

    EXPECT_TRUE(parse_uint(&parsed, "1"));
    EXPECT_EQ(parsed, 1);

    EXPECT_TRUE(parse_uint(&parsed, "10"));
    EXPECT_EQ(parsed, 10);

    EXPECT_TRUE(parse_uint(&parsed, "19"));
    EXPECT_EQ(parsed, 19);

    EXPECT_TRUE(parse_uint(&parsed, "0342520"));
    EXPECT_EQ(parsed, 342520);

    parsed = 0;
    EXPECT_TRUE(parse_uint(&parsed, "342520"));
    EXPECT_EQ(parsed, 342520);

    EXPECT_TRUE(parse_uint(&parsed, "4294967295"));
    EXPECT_EQ(parsed, 4294967295);

    parsed = 0;
    EXPECT_TRUE(parse_uint(&parsed, "0004294967295"));
    EXPECT_EQ(parsed, 4294967295);
}

TEST(parse_uint, invalid_numbers) {
    uint32_t parsed = 42;

    EXPECT_FALSE(parse_uint(&parsed, ""));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, " 0"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "-12"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, " 12"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "12 "));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "23 212"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "235$"));
    EXPECT_EQ(parsed, 42);
}

TEST(parse_uint, overflow) {
    uint32_t parsed = 42;

    EXPECT_FALSE(parse_uint(&parsed, "10000000000000"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "10000000000"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "4294967296"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "04294967296"));
    EXPECT_EQ(parsed, 42);

    EXPECT_FALSE(parse_uint(&parsed, "18446744073709551617"));
    EXPECT_EQ(parsed, 42);
}
