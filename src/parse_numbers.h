#ifndef UEBUNG03_STREQUAL_H
#define UEBUNG03_STREQUAL_H

#include <stdbool.h>
#include <stdint.h>

bool parse_uint(uint32_t* value, const char* s);

#endif  // UEBUNG03_STREQUAL_H
