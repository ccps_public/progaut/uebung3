extern "C" {
#include "sieve_of_eratosthenes.h"
}
#include <gtest/gtest.h>

TEST(sieve_of_eratosthenes, primes) {
    bool primes[1001];

    // Set some entries explicitely to the wrong values to check if the
    // function doesn't expect a initialization with false or true.
    primes[0] = true;
    primes[1] = true;
    primes[2] = false,
    primes[3] = false;

    // the function should not touch this value, as it is called with
    // size = 1000
    primes[1000] = true;

    sieve_of_eratosthenes(primes, 1000);
    EXPECT_TRUE(primes[2]);
    EXPECT_TRUE(primes[3]);
    EXPECT_TRUE(primes[5]);
    EXPECT_TRUE(primes[11]);
    EXPECT_TRUE(primes[19]);
    EXPECT_TRUE(primes[43]);
    EXPECT_TRUE(primes[53]);
    EXPECT_TRUE(primes[67]);
    EXPECT_TRUE(primes[79]);
    EXPECT_TRUE(primes[97]);
    EXPECT_TRUE(primes[997]);
    EXPECT_TRUE(primes[947]);

    EXPECT_FALSE(primes[0]);
    EXPECT_FALSE(primes[1]);
    EXPECT_FALSE(primes[4]);
    EXPECT_FALSE(primes[15]);
    EXPECT_FALSE(primes[91]);
    EXPECT_FALSE(primes[42]);
    EXPECT_FALSE(primes[33]);
    EXPECT_FALSE(primes[998]);
    EXPECT_FALSE(primes[999]);

    // check if this entry wasn't touched
    EXPECT_TRUE(primes[1000]);
}
