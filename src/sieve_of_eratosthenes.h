#ifndef UEBUNG03_SIEVE_OF_ERATOSTHENES_H
#define UEBUNG03_SIEVE_OF_ERATOSTHENES_H

#include "stdbool.h"

void sieve_of_eratosthenes(bool* data, int size);

#endif  // UEBUNG03_SIEVE_OF_ERATOSTHENES_H
