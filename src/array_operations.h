#ifndef UEBUNG03_ARRAY_OPERATIONS_H
#define UEBUNG03_ARRAY_OPERATIONS_H

void findMinMax(int* minidx, int* maxidx, const int* array, int size);
void limit(int* array, int size, int limit);

#endif  // UEBUNG03_ARRAY_OPERATIONS_H
