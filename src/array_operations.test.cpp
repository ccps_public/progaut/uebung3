extern "C" {
#include "array_operations.h"
}
#include <gtest/gtest.h>

TEST(array_operations, findMinMax) {
    int minidx = -1;
    int maxidx = -1;

    const int testArray1[] = {9, 0, 2, 3};
    findMinMax(&minidx, &maxidx, testArray1, 4);
    EXPECT_EQ(minidx, 1);
    EXPECT_EQ(maxidx, 0);

    minidx = -1;
    maxidx = -1;
    const int testArray2[] = {9, 12, 2, 3};
    findMinMax(&minidx, &maxidx, testArray2, 4);
    EXPECT_EQ(minidx, 2);
    EXPECT_EQ(maxidx, 1);

    minidx = -1;
    maxidx = -1;
    const int testArray3[] = {0, 12, 2, 3};
    findMinMax(&minidx, &maxidx, testArray3, 4);
    EXPECT_EQ(minidx, 0);
    EXPECT_EQ(maxidx, 1);

    minidx = -1;
    maxidx = -1;
    const int testArray4[] = {0, 12, 2, -2};
    findMinMax(&minidx, &maxidx, testArray4, 4);
    EXPECT_EQ(minidx, 3);
    EXPECT_EQ(maxidx, 1);

    minidx = -1;
    maxidx = -1;
    const int testArray5[] = {9, -2, 2, -2};
    findMinMax(&minidx, &maxidx, testArray5, 4);
    EXPECT_EQ(minidx, 1);
    EXPECT_EQ(maxidx, 0);

    minidx = -1;
    maxidx = -1;
    const int testArray6[] = {0, 12, 2, 22};
    findMinMax(&minidx, &maxidx, testArray6, 4);
    EXPECT_EQ(minidx, 0);
    EXPECT_EQ(maxidx, 3);
}

TEST(array_operations, limit) {
    int testArray[] = {9, 0, -12, 18, -18};
    const int result[] = {9, 0, -10, 10, -10};

    limit(testArray, 5, 10);

    for (int i = 0; i < 5; ++i) {
        EXPECT_EQ(testArray[i], result[i]);
    }
}
