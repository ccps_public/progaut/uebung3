extern "C" {
#include "strequal.h"
}
#include <gtest/gtest.h>

TEST(strequal, Strings_equal) {
    EXPECT_TRUE(strequal("Darmstadt", "Darmstadt"));
    EXPECT_TRUE(strequal("C ist TOLL", "C ist TOLL"));
    EXPECT_TRUE(strequal(" C ist TOLL!", " C ist TOLL!"));
    EXPECT_TRUE(strequal("!\"§$%&", "!\"§$%&"));
    EXPECT_TRUE(strequal("", ""));
}

TEST(strequal, Strings_not_equal) {
    EXPECT_FALSE(strequal("Darmstadt", "DaRmStAdT"));
    EXPECT_FALSE(strequal("C ist TOLL", "c iST toll"));
    EXPECT_FALSE(strequal(" C ist TOLL!", " c iST toll!"));

    EXPECT_FALSE(strequal("Darmstadt", "Berlin"));
    EXPECT_FALSE(strequal("Darmstadt", "Darmstadt "));
    EXPECT_FALSE(strequal(" ", ""));
}

TEST(strequali, Strings_equal_i) {
    EXPECT_TRUE(strequal("Darmstadt", "Darmstadt"));
    EXPECT_TRUE(strequal("C ist TOLL", "C ist TOLL"));
    EXPECT_TRUE(strequal(" C ist TOLL!", " C ist TOLL!"));
    EXPECT_TRUE(strequal("!\"§$%&", "!\"§$%&"));

    EXPECT_TRUE(strequali("Darmstadt", "DaRmStAdT"));
    EXPECT_TRUE(strequali("C ist TOLL", "c iST toll"));
    EXPECT_TRUE(strequali(" C ist TOLL!", " c iST toll!"));
    EXPECT_TRUE(strequali("", ""));
}

TEST(strequali, Strings_not_equal_i) {
    EXPECT_FALSE(strequali("Darmstadt", "Berlin"));
    EXPECT_FALSE(strequali("Darmstadt", "Darmstadt "));
    EXPECT_FALSE(strequali(" ", ""));
}
